BOT_NAME = "basic_spider"

SPIDER_MODULES = ["basic_spider.spiders"]
NEWSPIDER_MODULE = "basic_spider.spiders"

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = "Scrapy-Challenge-1 (17mayanksinghal@gmail.com)"

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

FEEDS = {
    "result.jl": {
        "format": "jsonlines",  # To support large data set
        "encoding": "utf8",
        "store_empty": False,
        "overwrite": True,
    },
}

SPIDER_MIDDLEWARES = {"scrapy_autounit.AutounitMiddleware": 950}

# Only enable when fixtures are to be updated
# AUTOUNIT_ENABLED = True
