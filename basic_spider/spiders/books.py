from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from basic_spider.items import BookItem


class BooksSpider(CrawlSpider):
    name = "books"
    allowed_domains = ["books.toscrape.com"]
    start_urls = ["http://books.toscrape.com/"]

    rules = (
        Rule(
            LinkExtractor(
                allow=r"catalogue/category/books/.*/index\.html",
            ),
            callback="parse_item",
            follow=False,
        ),
    )

    def parse_item(self, response):
        for book_tile in response.css(".product_pod"):
            book = BookItem()
            book["title"] = book_tile.css("h3 > a::attr(title)").extract_first()
            book["price"] = book_tile.css(
                ".product_price .price_color::text"
            ).extract_first()
            book["image_url"] = response.urljoin(
                book_tile.css(".image_container .thumbnail::attr(src)").extract_first()
            )
            book["details_page_url"] = response.urljoin(
                book_tile.css("h3 > a ::attr(href)").extract_first()
            )
            yield book

        next_page = response.css(".next > a::attr(href)").extract_first()
        if next_page:
            yield response.follow(next_page, callback=self.parse_item)
